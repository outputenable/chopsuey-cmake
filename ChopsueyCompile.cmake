function (chopsuey_target_compile_settings _target)
  if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options (${_target}
      PRIVATE
        -Wall -Wextra -Wpedantic
        -Wshadow
        -Wconversion -Wsign-conversion
        -Wzero-as-null-pointer-constant
        -Wswitch-enum
        -fstrict-aliasing -Wstrict-aliasing
        -fstrict-overflow -Wstrict-overflow
        -fno-guess-branch-probability)
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    target_compile_options (${_target}
      PRIVATE
        -W4
        -w34287   # 'operator' : unsigned/negative constant mismatch
        -w44062   # enumerator 'identifier' in switch of enum 'enumeration'
                  # is not handled
        -w44242   # 'identifier' : conversion from 'type1' to 'type2',
                  # possible loss of data
        -w44365)  # 'action' : conversion from 'type_1' to 'type_2',
                  # signed/unsigned mismatch
    # suppress min and max macro definitions
    target_compile_definitions (${_target} PUBLIC NOMINMAX)
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    target_compile_options (${_target}
      PRIVATE
        -Weverything
        -Wno-c++98-compat
        -Wno-c++98-compat-pedantic
        -Wno-c++98-c++11-compat-binary-literal
        -Wno-padded
        -Wno-global-constructors
        -Wno-exit-time-destructors
        -Wno-documentation)
          # due to Bug 24881 [-Wdocumentation] spurious `warning: empty
          # paragraph passed to '\returns' command`,
          # https://bugs.llvm.org/show_bug.cgi?id=24881
  endif ()
endfunction (chopsuey_target_compile_settings)

function (chopsuey_target_compile_warnings_as_errors _target)
  if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options (${_target} PRIVATE -Werror)
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    target_compile_options (${_target} PRIVATE -WX)
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    target_compile_options (${_target} PRIVATE -Werror)
  endif ()
endfunction (chopsuey_target_compile_warnings_as_errors)
